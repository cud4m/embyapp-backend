from maskerade.settings_global import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'embydb',
        'USER': 'emby',
        'PASSWORD': 'P4ss-_-',
        'HOST': 'localhost',
        'PORT': '5435',                      # Set to empty string for default.
    }
}

DEBUG = False

ALLOWED_HOSTS = ['api.embyapp.com']

STATIC_URL = '/static/'
