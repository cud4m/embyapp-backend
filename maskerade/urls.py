"""maskerade URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.core.cache import cache
from django.urls import path, include, re_path
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet
from rest_framework.routers import DefaultRouter

from maskerade import views
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
routerFCM = DefaultRouter()

routerFCM.register('devices', FCMDeviceAuthorizedViewSet)
cache.delete('picture/showoff')

schema_view = get_schema_view(
   openapi.Info(
      title="emby API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)
urlpatterns = [
    path('', include('django_backblaze_b2.urls')),
    path('', include(routerFCM.urls)),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('auth/create/', views.UserCreateAPIView.as_view()),
    path('picture/upload/avatar/', views.UpdateProfilePicture.as_view()),
    path('picture/upload/showoff/', views.AddShowOffPicture.as_view()),
    path('picture/upload/survey/', views.AddSurveyPicture.as_view()),
    re_path('picture/survey/remove/(?P<pk>[0-9]+)/$', views.RemoveSurveyPicture.as_view()),
    re_path('picture/profile/remove/(?P<pk>[0-9]+)/$', views.RemoveProfilePicture.as_view()),
    re_path('picture/showoff/(?P<pk>[0-9]+)/$', views.GetShowOffPicture.as_view()),
    path('picture/surveys/', views.GetSurveyPicture.as_view()),
    path('download/auth/', views.AuthorizeDownload.as_view()),
    path('friends/', views.UserFriendsView.as_view()),
    path('friends/request/', views.FriendRequest.as_view()),
    path('friends/add/', views.AddFriendView.as_view()),
    path('friends/chat/', views.OpenNewChat.as_view()),
    re_path('friends/reject-or-remove/', views.RejectRemoveFriendView.as_view()),
    path('admin/', admin.site.urls),
    re_path(r'check_password$', views.ChatAuthView.as_view()),
    re_path(r'user_exists$', views.UserExistView.as_view()),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('surveys/', include('survey.urls')),
    re_path(r'users/search/(?P<username>[a-zA-Z0-9@.+\-]+)/$', views.UserSearchView.as_view(),
            name='friend-search'),

]
