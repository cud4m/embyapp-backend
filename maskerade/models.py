from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User, PermissionsMixin, UserManager
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.utils import timezone
from django_backblaze_b2 import BackblazeB2Storage
from .formatChecker import ContentTypeRestrictedFileField

from survey.models import Option, Survey, Localize

GENDER_CHOICES = (
    ('M', 'male'),
    ('W', 'female'),
    ('O', 'other'),
)

LANGUAGES = (
    ('es', 'spanish'),
    ('en', 'english')
)


class ProfileManager(UserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


"""
    Same as FileField, but you can specify:
        * content_types - list containing allowed content_types. Example: ['application/pdf', 'image/jpeg']
        * max_upload_size - a number indicating the maximum file size allowed for upload.
            2.5MB - 2621440
            5MB - 5242880
            10MB - 10485760
            20MB - 20971520
            50MB - 5242880
            100MB - 104857600
            250MB - 214958080
            500MB - 429916160
"""


class Profile(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()

    username = models.CharField( max_length=18, unique=True, )
    email = models.EmailField(unique=True, max_length=254, )
    password = models.CharField(max_length=250, )
    avatar = ContentTypeRestrictedFileField(content_types=['image/jpeg', 'image/jpg'], max_upload_size=5242880,
                                            upload_to='avatars/', blank=True, null=True, storage=BackblazeB2Storage, )
    gender = models.CharField(max_length=1, default='W', choices=GENDER_CHOICES)
    birthday = models.DateField(default=timezone.now)
    language = models.ManyToManyField(Localize),
    date_joined = models.DateTimeField(default=timezone.now)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    options = models.ManyToManyField(Option, blank=True)
    is_active = models.BooleanField(default=True)
    friends = models.ManyToManyField('Profile')
    bio = models.TextField(max_length=5000)


    UNIQUE_FIELDS = 'username'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['password', 'email']

    def save(self, *args, **kwargs):
        self.username = self.username.lower()
        return super(Profile, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'Profile of user: {0}'.format(self.email)


class ShowOffPicture(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    image = ContentTypeRestrictedFileField(content_types=['image/jpeg', 'image/jpg'], max_upload_size=5242880,
                                           upload_to='showoff/', storage=BackblazeB2Storage, )

    def __str__(self):
        return self.profile.username + ' - ' + self.image.name


class SurveyPicture(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True)
    image = ContentTypeRestrictedFileField(content_types=['image/jpeg', 'image/jpg', 'image/gif'], max_upload_size=5242880,
                                           upload_to='surveys/', storage=BackblazeB2Storage, )

    def __str__(self):
        return self.profile.username if self.profile is not None else '' + ' - ' + self.image.name


