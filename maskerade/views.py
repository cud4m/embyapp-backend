from b2sdk.api import B2Api
from django.utils import timezone
from fcm_django.models import FCMDevice
from oauth2_provider.models import AccessToken
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListCreateAPIView, ListAPIView, DestroyAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from maskerade.models import Profile, ShowOffPicture, SurveyPicture
from maskerade.serializers import ProfileSerializer, PublicProfileSerializer

from django_backblaze_b2 import BackblazeB2Storage

from survey.models import Question
from survey.serializers import ShowOffPictureSerializer, SurveyPictureSerializer

client_id = "tYsMKVrKxiRtuYAzv7mki694kGiIHrGbqWagQRPn"
client_secret = "ofIsnI2MnDVFHUp5BJ9Zwu0xbHH0HVuZds0i8nN1Ix9jAq8MiXrvq6BqnDGZHFSVNsfVYZcvsiLuDSgG6WCsS8aw9i0PwFrccuGKrUoDFrkCDEyQ30yBvzz42UUfu72Y"


from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from oauth2_provider.models import Application


class OAuth2ClientCredentialAuthentication(OAuth2Authentication):
    """
    OAuth2Authentication doesn't allows credentials to belong to an application (client).
    This override authenticates server-to-server requests, using client_credential authentication.
    """
    def authenticate(self, request):
        authentication = super().authenticate(request)

        if not authentication is None:
            user, access_token = authentication
            if self._grant_type_is_client_credentials(access_token):
                authentication = access_token.application.user, access_token

        return authentication

    def _grant_type_is_client_credentials(self, access_token):
        return access_token.application.authorization_grant_type == Application.GRANT_CLIENT_CREDENTIALS


class UserCreateAPIView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ProfileSerializer


class UserFriendsView(ListAPIView):
    serializer_class = PublicProfileSerializer

    def get_queryset(self):
        request_user = Profile.objects.get(id=self.request.user.id)

        request_user_followers = Profile.objects.filter(friends__id=self.request.user.id)
        return request_user_followers & request_user.friends.all()


class FriendRequestView(ListAPIView):
    serializer_class = PublicProfileSerializer

    def get_queryset(self):
        request_user = Profile.objects.get(id=self.request.user.id)
        request_user_followers = Profile.objects.filter(friends__id=self.request.user.id)
        return request_user_followers.exclude(request_user.friends.all())


class AddFriendView(CreateAPIView):
    serializer_class = PublicProfileSerializer

    def get(self, request, *args, **kwargs):

       # device.send_message("Title", "Message")
       # device.send_message(data={"test": "test"})
        #device.send_message(title="You have a new friend request!", body="Message", icon='assets/images/icons/emby_isotipo.png', data={"test": "test"})
        return Response({'ok': 'ok'})
    # TODO MY EYES ARE IN BLOOD TEARS
    def post(self, request, *args, **kwargs):

        req_user = Profile.objects.filter(id=self.request.data['id'])
        loged_user = Profile.objects.filter(id=self.request.user.id, friends__id=self.request.data['id'])
        if req_user.exists() and not loged_user.exists():

            self.request.user.friends.add(req_user[0].id)
            devices = FCMDevice.objects.all().filter(user=req_user[0], active=True)

            if (req_user[0].friends.filter(friends__id=self.request.user.id)):
                devices.send_message(title="You have a new friend request!", body="{0} has wants to be your friend!".format( self.request.user.username),
                                    icon='/assets/images/icons/emby_isotipo.png', data={"view": "friend_profile",
                                                                                       "profile": PublicProfileSerializer(
                                                                                           self.request.user,
                                                                                           context={
                                                                                               'request': self.request,
                                                                                               'kwargs': self.kwargs})
                                     .data})
            else:
                devices.send_message(title="You have a new friend !",
                                     body="{0} has accepted your friend request!".format(self.request.user.username),
                                     icon='/assets/images/icons/emby_isotipo.png', data={"view": "friend_profile",
                                                                                         "profile": PublicProfileSerializer(
                                                                                             self.request.user,
                                                                                             context={
                                                                                                 'request': self.request,
                                                                                                 'kwargs': self.kwargs})
                                     .data})

            return Response({'status': 'Friend successfully added'})
        return Response({'status': 'Friend not added'}, status=status.HTTP_409_CONFLICT)


class RejectRemoveFriendView(CreateAPIView):
    serializer_class = PublicProfileSerializer

    # TODO MY EYES ARE IN BLOOD TEARS
    def post(self, request, *args, **kwargs):
        try:
            profile = Profile.objects.get(id=self.request.POST.get('id'))
            profile.friends.remove(self.request.user)
            print('their remove')
            self.request.user.friends.remove(profile)
            print("my remove")
            return Response({'status': 'Friend removed successfully'}, status=status.HTTP_200_OK)
        except Exception:
           return Response({'status': 'Friend not removed'}, status=status.HTTP_304_NOT_MODIFIED)




class UserSearchView(ListAPIView):
    serializer_class = PublicProfileSerializer

    def get_queryset(self):
        return Profile.objects.filter(username__icontains=self.kwargs['username'])[:50]


class FriendRequest(ListAPIView):
    serializer_class = PublicProfileSerializer

    def get_queryset(self):
        request_user = Profile.objects.get(id=self.request.user.id)
        request_user_followers = Profile.objects.filter(friends__id=self.request.user.id).all()
        return request_user_followers.difference(request_user.friends.all())


class ChatAuthView(APIView):
    permission_classes = [AllowAny]

    def get(self, *args, **kwargs):
        try:
            remote_addr = self.request.META['REMOTE_ADDR']
            print(remote_addr)
            if remote_addr != '37.187.114.224':
                # if remote_addr == '192.168.1.209':
                user = self.request.GET.get('user')
                token = self.request.GET.get('pass')
                AccessToken.objects.get(token__exact=token, user__username=user, expires__gt=timezone.now())
                return Response(True, status=status.HTTP_200_OK)
            else:
                return Response({"status": "Invalid IP"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"status": e.args}, status=status.HTTP_400_BAD_REQUEST)


class UserExistView(APIView):
    permission_classes = [AllowAny]

    def get(self, *args, **kwargs):
        try:
            remote_addr = self.request.META['REMOTE_ADDR']

            if remote_addr != '37.187.114.224':
                # if remote_addr == '192.168.1.209':
                user = self.request.GET.get('user')

                Profile.objects.get(username=user)
                return Response(True, status=status.HTTP_200_OK)
            else:
                return Response({"status": "Invalid IP"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"status": e.args}, status=status.HTTP_400_BAD_REQUEST)


class UpdateProfilePicture(APIView):
    serializer_class = ProfileSerializer

    def get_queryset(self):
        return Profile.objects.filter(id=self.request.user.id).all()

    def post(self, request, *args, **kwargs):
        request_user = Profile.objects.get(id=self.request.user.id)
        request_user.avatar = self.request.data['image']
        request_user.save()
        return Response({"status":  'ok',
                         "avatar_url": self.request.user.avatar.url
                         }, status=status.HTTP_200_OK)


class AddShowOffPicture(APIView):

    #  def get_queryset(self):
    #      return Profile.objects.filter(id=self.request.user.id).all()

    def post(self, request, *args, **kwargs):
        image = ShowOffPicture.objects.create(
            profile_id=self.request.user.id,
            image=self.request.data['image']
        )

        return Response({"id": image.id, }, status=status.HTTP_200_OK)


class AddSurveyPicture(APIView):

    #  def get_queryset(self):
    #      return Profile.objects.filter(id=self.request.user.id).all()

    def post(self, request, *args, **kwargs):
        image = SurveyPicture.objects.create(
             profile_id=self.request.user.id,
             image=self.request.data['image']
         )
        return Response({"id": image.id, }, status=status.HTTP_200_OK)


class OpenNewChat(APIView):
    #  def get_queryset(self):
    #      return Profile.objects.filter(id=self.request.user.id).all()

    def post(self, request, *args, **kwargs):
        req_user = Profile.objects.filter(id=self.request.data['id'])
        if req_user.exists():
            devices = FCMDevice.objects.all().filter(user=req_user[0], active=True)
            if (req_user[0].friends.filter(friends__id=self.request.user.id)):
                devices.send_message(title="People are talking to you!",
                                     tag="chat",
                                     body="{0} has sent you a message!".format(self.request.user.username),
                                     icon='/assets/images/icons/emby_isotipo.png', data={"view": "chat",
                                                                                         "profile": PublicProfileSerializer(
                                                                                             self.request.user,
                                                                                             context={
                                                                                                 'request': self.request,
                                                                                                 'kwargs': self.kwargs})
                                     .data})
            return Response({'status': 'ok'})
        return Response({'status': 'error'}, status=status.HTTP_409_CONFLICT)


class RemoveProfilePicture(DestroyAPIView):
    serializer_class = ShowOffPictureSerializer

    def get_queryset(self):
        print(self.request.data)
        return ShowOffPicture.objects.filter(
            profile_id=self.request.user.id,
            id=self.kwargs['pk']
        )


class RemoveSurveyPicture(DestroyAPIView):
    serializer_class = SurveyPictureSerializer

    def get_queryset(self):
            image = SurveyPicture.objects.filter(
                profile_id=self.request.user.id,
                id=self.kwargs['pk']
            )
            if not image.exists():
                return []
            if not Question.objects.filter(picture__in=image).exists():
                return image
            else:
                sel = image.first()
                sel.profile = None
                sel.save()
                return []


class AuthorizeDownload(APIView):

    def get(self, request, *args, **kwargs):
        applicationKey = "K0003VTJNXFKd0py1l1M4kHHLIFsL7Y"
        keyID = "000b1c3b9bd29c20000000002"
        B2Api().authorize_account('production', application_key_id=keyID, application_key=applicationKey)
        return Response({"token": str(B2Api().account_info.get_account_auth_token())}, status=status.HTTP_200_OK)


class GetShowOffPicture(ListAPIView):
    serializer_class = ShowOffPictureSerializer

    def get_queryset(self):
        return ShowOffPicture.objects.filter(profile__id=self.kwargs.get('pk')).order_by('-id').all()


class GetSurveyPicture(ListAPIView):
    serializer_class = SurveyPictureSerializer

    def get_queryset(self):
        return SurveyPicture.objects.filter(profile__id=self.request.user.id).order_by('-id').all()
