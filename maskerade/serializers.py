from django.db import transaction
from rest_framework import serializers
from rest_framework.utils import json

from maskerade.models import Profile
from survey.models import Localize


class LocalizeSerializer(serializers.ModelSerializer):
    lang = serializers.CharField(read_only=True)

    class Meta:
        model = Localize
        fields = ('id', 'lang')


class AvatarSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()

    def get_avatar(self, profile):
        return profile.avatar.name
    class Meta:
        model = Profile
        fields = ('avatar')


class ProfileSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    language = LocalizeSerializer(many=True, read_only=True)
    localize = serializers.ListField(write_only=True,  child=serializers.CharField())

    avatar = serializers.SerializerMethodField()

    def get_avatar(self, profile):
        return profile.avatar.name


    class Meta:
        model = Profile
        fields = ('id', 'language', 'avatar', 'gender', 'birthday', 'email',  'password',  'username', 'localize')

    def create(self, validated_data):
        with transaction.atomic():

            languages = validated_data.pop('localize', None)
            user = Profile(**validated_data)
            # ensure username is lowercas to avoid problems when xmpp try to to authenticate
            user.username = user.username.lower()
            if len(user.username) < 4:
                raise serializers.ValidationError({"status": 'Username must have more than 3 chars'})

            if Profile.objects.filter(username=user.username):
                raise serializers.ValidationError({"status": 'Username already taken'})
            user.set_password(validated_data['password'])
            languages = Localize.objects.filter(id__in=json.loads(languages[0])).all()
            user.language = languages
            user.save()
            #user = Profile.objects.get(username=validated_data.__getitem__('username'))

        return user


class PublicProfileSerializer(serializers.ModelSerializer):
    your_request = serializers.SerializerMethodField()

    def get_your_request(self, profile):
        req_user = self.context['request'].user
        return req_user.friends.filter(id=profile.id).all().exists()

    their_request = serializers.SerializerMethodField()

    def get_their_request(self, profile):
        req_user = self.context['request'].user
        return profile.friends.filter(id=req_user.id).all().exists()

    avatar = serializers.SerializerMethodField()

    def get_avatar(self, profile):
        return profile.avatar.name

    class Meta:
        model = Profile
        fields = ('id', 'username', 'avatar', 'your_request', 'their_request')




