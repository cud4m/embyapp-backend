from django.contrib import admin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.utils.safestring import mark_safe

from maskerade.models import Profile,  ShowOffPicture, SurveyPicture
from django import forms
from django.core.exceptions import ValidationError

from survey.models import Localize


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Profile
        fields = ('email', 'birthday', 'username')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Profile
        fields = ('email', 'password', 'birthday', 'is_active', 'is_superuser', 'username')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm


    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'birthday', 'is_superuser',  )
    list_filter = ('is_superuser', 'username')
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password', 'avatar',  'friends', 'options', )}),
        ('Personal info', {'fields': ('birthday',)}),
        ('Permissions', {'fields': ('is_superuser',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'birthday', 'password1', 'password2', 'username'),
        }),
    )
    search_fields = ('email', 'username')
    ordering = ('email', 'username')
    filter_horizontal = ()


# @admin.register(SurveyPicture)
# class SurveyPictureAdmin(admin.ModelAdmin,):
#
#     readonly_fields = ["headshot_image"]
#
#     def headshot_image(self, obj):
#         return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
#             url=obj.image.url,
#             width=obj.image.width,
#             height=obj.image.height,
#             )
#     )

admin.site.unregister(Group)
admin.site.register(Profile, UserAdmin)
admin.site.register(Localize)
admin.site.register(ShowOffPicture)
admin.site.register(SurveyPicture)
