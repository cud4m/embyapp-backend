from django.db import models

# Create your models here.
from django.utils import timezone

from maskerade.models import Profile

PRIZE_TYPES = (
    ('0', 'Text'),
    ('1', 'Multimedia'),
)
GAME_TYPES = (
    ('0', 'TrueAndLie'),
    ('1', 'Queharia'),
)


class ChatGame(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    creator_user = models.ForeignKey(Profile, related_name='creator_user', on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)
    prize_type = models.CharField(max_length=11, default='0', choices=PRIZE_TYPES)
    game_type = models.CharField(max_length=11, default='0', choices=GAME_TYPES)


class ChatGameAnswer(models.Model):
    game = models.ForeignKey(ChatGame, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)