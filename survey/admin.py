from django.contrib import admin

# Register your models here.
from survey.models import Survey, Question,  Option

admin.site.register(Survey)
admin.site.register(Question)
admin.site.register(Option)

