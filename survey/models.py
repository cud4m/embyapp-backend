from django.db import models
from django.utils import timezone


class Localize(models.Model):
    lang = models.CharField(max_length=20, default='English')

    def __str__(self):
        return self.lang


SURVEY_VISIBILITY = (
    ('public', 'public',),
    ('private', 'private'),
    ('link', 'link'),
)


class Survey(models.Model):
    created_by = models.ForeignKey('maskerade.Profile', related_name='created_by', on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=50, default="survey")
    lang = models.ForeignKey(Localize, default=1, on_delete=models.CASCADE)
    visibility = models.CharField(default='public', max_length=8)
    date_created = models.DateTimeField(default=timezone.now)
    # counter to increment each time someone select this option
    counter = models.IntegerField(default=0)
    tags = models.ManyToManyField('Tags', blank=True)


    def __str__(self):
        return self.title



QUESTION_TYPES = (
    ('shortAnswer', 'shortAnswer',),
    ('singleChoice', 'singleChoice'),
    ('multipleChoice', 'multipleChoice'),
    ('linearScale', 'linearScale'),
    ('linearScale', 'linearScale'),
    ('linearScaleGrid', 'linearScaleGrid')
)


class Question(models.Model):
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE)
    title = models.TextField()
    type = models.CharField(choices=QUESTION_TYPES, default='singleChoice', max_length=15)
    picture = models.ForeignKey('maskerade.SurveyPicture', blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.title + " " + self.type


class Option(models.Model):
    title = models.CharField(max_length=250)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    otherSpecify = models.BooleanField(default=False)
    # counter to increment each time someone select this option
    counter = models.IntegerField(default=0)

    def __str__(self):
        return self.title + " - " + self.question.survey.title


class Tags(models.Model):
    name = models.CharField(max_length=20, editable=False)


