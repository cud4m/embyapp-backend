import math
from itertools import islice

from django.db import transaction
from django.db.models import Q, Count
from django.template.defaultfilters import lower
from fcm_django.models import FCMDevice
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer

from maskerade.models import Profile, ShowOffPicture, SurveyPicture
from maskerade.serializers import LocalizeSerializer, PublicProfileSerializer
from survey.models import Survey, Option, Question, Tags


class SurveyResultSerializer(ModelSerializer):
    survey_id = serializers.IntegerField()
    options = serializers.ListField(child=serializers.IntegerField())

    class Meta:
        model = Survey
        fields = ('survey_id', 'options')


    def create(self, validated_data):
        with transaction.atomic():

            raw_options = validated_data.pop('options', None)
            survey_id = validated_data.pop('survey_id', None)
            # self.context['request'].user.id
            request_user= Profile.objects.get(id=self.context['request'].user.id)
            # get and check that all options exist for that survey (NOT INCLUDING SAME OR DIF QUESTION)
            model_opts = Option.objects.filter(id__in=raw_options, question__survey__id=survey_id)
           # print(model_opts.values('question__id').distinct())
            # check that the user haven't previously completed the survey and that all opts exist for that surv
            # and ensure that the questions are difference CAUSE RN ONLY SINGLECHOICE AVAILABE

            if len(model_opts.values('question__id').distinct()) != len(raw_options) or not model_opts.exists() or \
                    request_user.options\
                    .filter(profile__options__in=model_opts).exists():

                raise serializers.ValidationError({"status": 'Survey id or Options id were wrong'})




           # n_questions_surv = Question.objects.filter(survey_id=survey_id).count()
            survey = Survey.objects.get(id=survey_id)
            survey.counter = survey.counter+1
            survey.save()

            #https://medium.com/swlh/efficient-bulk-create-with-django-rest-framework-f73da6af7ddc
            # get all profiles with the survey saved and it's options
            # add the survey to completed for the user
            request_user.options.add(*raw_options)
            # old query one way match
            #matching_profiles = Profile.objects.filter(options__id__in=model_opts, savedSurvey__id=survey_id).distinct().annotate(optCounts=Count('options',
        #filter=Q(options__id__in=model_opts))).exclude(id=request_user.id)


        #Match.save()
        return {'survey_id': survey_id, 'options': raw_options}


class ShowOffPictureSerializer(ModelSerializer):
    image = SerializerMethodField(read_only=True)

    def get_image(self, showoffpic):
        return showoffpic.image.name

    class Meta:
        model = ShowOffPicture
        fields = ('id', 'image',)


class SurveyPictureSerializer(ModelSerializer):
    image = serializers.SerializerMethodField(read_only=True)

    def get_image(self, surveypic):
        return surveypic.image.name

    class Meta:
        model = SurveyPicture
        fields = ('id', 'image',)


class QuestionSerializer(ModelSerializer):

    options = SerializerMethodField()
    picture = SurveyPictureSerializer(many=False)

    def get_options(self, question):
        return OptionSerializer(Option.objects.all().filter(question=question).all(),
                                many=True, context=self.context).data

    class Meta:
        model = Question
        fields = ('id', 'type', 'title',  'options', 'picture')


class OptionSerializer(ModelSerializer):
    selected = serializers.BooleanField(write_only=True)

    class Meta:
        model = Option
        fields = ('id', 'title', 'otherSpecify', 'selected')


class OptionIDSerializer(ModelSerializer):

    class Meta:
        model = Option
        fields = ('id', )


class SurveySerializer(ModelSerializer):
    questions = SerializerMethodField()
   # answered = serializers.IntegerField(read_only=True)
    language = LocalizeSerializer(many=True, read_only=True)
    created_by = PublicProfileSerializer(many=False, read_only=True)
    #
    # def get_answered(self, survey):
    #     print(survey)
    #     return Survey.objects.filter(qu)

    def get_questions(self, survey):
        serial_data = QuestionSerializer(survey.question_set, many=True, context=self.context)
        return serial_data.data

    class Meta:
        model = Survey
        fields = ('id', 'title', 'questions', 'language', 'created_by')


class SurveyInfoSerializer(ModelSerializer):
    lang = LocalizeSerializer(many=True).data

    class Meta:
        model = Survey
        fields = ('id', 'title', 'lang')

#
# class CoincidenceSerializer(ModelSerializer):
#     survey = SerializerMethodField()
#
#     def get_survey(self, coincidence):
#         serial_data = SurveySerializer(coincidence.survey, many=False,
#                                               context=self.context)
#         return serial_data.data
#
#     class Meta:
#         model = Match
#         fields = ('id', 'survey', 'percentage',)


class CoincidenceSerializer(ModelSerializer):
    #survey = SurveyInfoSerializer( many=False)
    your_request = SerializerMethodField()

    def get_your_request(self, profile):
        req_user = self.context['request'].user
        return req_user.friends.filter(id=profile.id).all().exists()

    their_request = serializers.SerializerMethodField()

    def get_their_request(self, profile):
        req_user = self.context['request'].user
        return profile.friends.filter(id=req_user.id).all().exists()

    avatar = serializers.SerializerMethodField()

    def get_avatar(self, profile):
        return profile.avatar.name

    percentage = serializers.SerializerMethodField()

    def get_percentage(self, profile ):
        survey_id = self.context['kwargs']['pk']
        options_for_match = self.context['request'].user.options.filter(question__survey__id=survey_id).all().values_list('id', flat=True)
        n_questions_surv = Question.objects.filter(survey_id=survey_id).count()
        count = profile.options.all().filter(
            id__in=options_for_match,)\
            .annotate(optCounts=Count('id', filter=Q(id__in=options_for_match))).count()
        # print(matching_profiles)
        # self.context['request'].user.id
        return int(math.floor(count / n_questions_surv * 100))

    class Meta:
        model = Profile
        fields = ('id', 'username', 'avatar', 'percentage', 'your_request', 'their_request')


class CreateOptionSerializer(ModelSerializer):

    class Meta:
        model = Option
        fields = '__all__'


class CreateQuestionSerializer(ModelSerializer):

    options = OptionSerializer(many=True)

    class Meta:
        model = Question
        fields = ('title', 'type', 'options', 'picture')

    def create(self, validated_data):
        with transaction.atomic():

            options = validated_data.pop('options', None)
            quest = Question(**validated_data)
            quest.save()
            opts = []
            for option in options:
               opts.append( Option.objects.create(question=quest, **option))

            quest.options = opts
        return quest


class TagSerializer(ModelSerializer):

    class Meta:
        print('eco')
        model = Tags
        fields = ('id', 'name')
        read_only_fields = ('id',)

    # def create(self, validated_data):
    #     with transaction.atomic():
    #
    #         tags = validated_data.pop('tags', None)
    #
    #         for tag in tags:
    #             formatted_tag = lower(' '.join(tag.split()))
    #             search = Tags.objects.filter(name__search=formatted_tag)
    #             if not search.exists():
    #                 Tags.objects.create(
    #                     title=tag
    #                 )
    #     return tags


class CreateSurveySerializer(ModelSerializer):
    tags = TagSerializer(many=True)
    questions = CreateQuestionSerializer(many=True)

    class Meta:
        model = Survey
        fields = ('title', 'questions', 'lang', 'visibility', 'tags')

    def create(self, validated_data):
        with transaction.atomic():

            questions = validated_data.pop('questions', None)
            tags=validated_data.pop('tags', None)
            surv = Survey(**validated_data)
            user_profile = Profile.objects.get(id=self.context['request'].user.id)

            surv.created_by = user_profile
            surv.save()

            #surv.savedBy.add(User.objects.get(id=1))
        # surv.save()
            #if len(tags) > 4:
            #    del tags[5, len(tags-1)]
            validated_tags_list = []



            # for tag in tags:
            #     print('tag')
            #     print(tag.values())
            #     for k, v in tag.items():
            #         print('maaap')
            #         print(k, v)
            #
            #     formatted_tag = lower(' '.join(tag.split()))
            #     search = Tags.objects.filter(name__search=formatted_tag)
            #     if not search.exists():
            #         validated_tags_list.append(Tags.objects.create(
            #             title=tag
            #         ))
            #
            # surv.tags.add(validated_tags_list)



            for question in questions:
                options = question["options"]
                del question["options"]
                added_question = Question.objects.create(survey=surv, **question)
                for option in options:
                    selected = option["selected"]
                    del option["selected"]
                    createdopt = Option.objects.create(question=added_question, **option)
                    if selected:
                        user_profile.options.add(createdopt)
            user_profile.save()

        if surv.visibility != 'link':
            devices = FCMDevice.objects.all().filter(user__in=self.context['request'].user.friends.all(), active=True)
            devices.send_message(title="{0} has created a new survey, check it out!".format(self.context['request'].user),
                                 tag="survey",
                                 body="{0}".format(questions[0]["title"]),
                                 icon='/assets/images/icons/emby_isotipo.png', data={"view": "survey",
                                                                                     "survey": SurveySerializer(
                                                                                         surv,
                                                                                         context=self.context).data})




        surv.questions = []
        return surv

