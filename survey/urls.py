from django.urls import path, re_path

from survey import views
from survey.views import FriendsSurveyMatch

urlpatterns = [
    path('', views.SurveyList.as_view()),
    path('tags/', views.TagList.as_view()),
    re_path('get/(?P<pk>[0-9]+)/$', views.GetSurveyView.as_view()),
    re_path('check_answers/(?P<survey_id>[0-9]+)/(?P<user_id>[0-9]+)/$', views.GetFriendAnswersView.as_view()),
    re_path('created_by/(?P<pk>[0-9]+)/$', views.GetUserCreatedSurveys.as_view()),
    re_path('completed_by/(?P<pk>[0-9]+)/$', views.GetUserCompletedSurveys.as_view()),
    re_path('coincidences/onlyfriends/(?P<pk>[0-9]+)/', FriendsSurveyMatch.as_view()),
    re_path('coincidences/(?P<pk>[0-9]+)/$', views.MatchList.as_view()),
    re_path('check_if_completed/(?P<pk>[0-9]+)/$', views.CheckIfCompleted.as_view()),
    path('new/', views.NewSurvey.as_view()),
    path('answer/', views.NewResult.as_view()),
    re_path('friends/(?P<pk>[0-9]+)/$', views.GetWhatFriendsHaveCompletedTheSurvey.as_view()),

    #path('match/', views.MatchList.as_view()),
   # path('answer/new/', views.NewAnswer.as_view())
]
