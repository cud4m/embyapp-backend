# Generated by Django 3.1.6 on 2021-04-11 19:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('maskerade', '0010_auto_20210411_1511'),
        ('survey', '0004_survey_visibility'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='picture',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='maskerade.surveypicture'),
        ),
    ]
