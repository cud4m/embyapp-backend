from django.db.models import Count, Q
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.views import APIView

from maskerade.models import Profile
from maskerade.serializers import ProfileSerializer, PublicProfileSerializer
from survey.models import Survey, Question, Option, Tags
from survey.serializers import SurveySerializer, CreateSurveySerializer, \
    SurveyResultSerializer, CoincidenceSerializer, OptionIDSerializer, TagSerializer
import random

class SurveyList(ListAPIView):
    # filter_backends = [DjangoFilterBackend]
    filterset_fields = ['tags', 'counter', 'date_created']
    serializer_class = SurveySerializer

    def get_queryset(self):
        #profile = Profile.objects.get(id=self.request.user.id)
        #completedsurveys = profile.options.prefetch_related().values_list('question__survey_id', flat=True)
        # .exclude(id__in=completedsurveys).order_by('-id')
        # lang__in=profile.language.values_list('id', flat=True)
        survey_queryset = Survey.objects.filter(~Q(question__option__profile__exact=self.request.user),
                                     ~Q(created_by=self.request.user),
                                     ~Q(lang__lang=self.request.user.language), visibility='public')
        random_id_list = [random.choice(survey_queryset.values_list('id', flat=True)) for _ in range(15)]
        return survey_queryset.filter(id__in=random_id_list)



class TagList(ListAPIView):
    # filter_backends = [DjangoFilterBackend]
    # filterset_fields = ['categories']
    serializer_class = TagSerializer
    queryset = Tags.objects.all()


class MatchList(ListAPIView):
    # filter_backends = [DjangoFilterBackend]
    # filterset_fields = ['categories']
    serializer_class = CoincidenceSerializer

    def get(self, request, *args, **kwarg):
        filter_names = ('friends',)
        # filter_clauses = [Q(filter=self.request.GET[filter])
        #                  for filter in filter_names
        #                  if self.request.GET.get(filter)]
        #

        # filter  all profiles that have a match
        options_for_match = self.request.user.options.filter(question__survey__id=self.kwargs['pk'])
        queryset = Profile.objects.filter(options__id__in=options_for_match).distinct().exclude(id=self.request.user.id)
            #.exclude(id__in=self.request.user.friends.values_list('id', flat=True))

        serializer = CoincidenceSerializer(queryset, many=True, context={'request': self.request, 'kwargs': self.kwargs})

        # TODO a reall wway to filter https://stackoverflow.com/questions/34739680/how-to-add-filters-to-a-query-dynamically-in-django
        # if filter_clauses:
        #     friends = Profile.objects.filter(friends__id=self.request.user.id).prefetch_related() & self.request.user.friends
        #     queryset = queryset.filter(user=self.request.user, survey_id=self.kwargs['pk']).order_by('-percentage') # queryset.filter(reduce(operator.and_, filter_clauses))

        #return queryset  # .filter(user=self.request.user,).order_by('-percentage')

        return Response(serializer.data)


class FriendsSurveyMatch(ListAPIView):
    serializer_class = CoincidenceSerializer
  #  def get_queryset(self, *args, **kwargs):
  #      return self.request.user.friends.filter(options__question__survey__id=self.kwargs['pk']).filter(friends__id=self.request.user.id)
    def get(self, request, *args, **kwarg):
        options_for_match = self.request.user.options.filter(question__survey__id=self.kwargs['pk'])
        queryset = self.request.user.friends.filter(options__id__in=options_for_match, friends__id=self.request.user.id)\
            .distinct().exclude(id=self.request.user.id)

        serializer = CoincidenceSerializer(queryset, many=True, context={'request': self.request, 'kwargs': self.kwargs})

        return Response(serializer.data)



## CreateOptionSerializer(data={'title': 'Esto es', 'question': '1asd', 'isCorrect': 'true', 'otherSpecify': 'false'}).is_valid()


class NewSurvey(CreateAPIView):
    queryset = Survey
    serializer_class = CreateSurveySerializer


class NewResult(CreateAPIView):
    queryset = Survey
    serializer_class = SurveyResultSerializer


class RemoveCompletedSurvey(CreateAPIView):
    queryset = Profile
    serializer_class = ProfileSerializer

    def post(self, *args, **kwargs):
        try:
            survey_id = self.request.data.get('id')
            # probably no t a good idea data santitation boy!!!
            # Anyway :) im asleep and want it done
            surv = Survey.objects.get(id=survey_id)
            if Profile.objects.get(self.request.user).savedSurvey.filter(surv).exists():
                Profile.objects.get(self.request.user).savedSurvey.remove(surv)
                return Response({"status": "Survey Removed"})
            return Response({"status": "Survey is not saved!"})
        except Exception as e:
            return Response({"status": e})



class GetSurveyView(ListAPIView):
    serializer_class = SurveySerializer

    def get_queryset(self):
        try:
            survey = Survey.objects.get(id=self.kwargs['pk'])
            return [survey]
        except Survey.DoesNotExist:
            return []


class GetFriendAnswersView(ListAPIView):
    serializer_class = OptionIDSerializer

    def get_queryset(self):
        try:
            # TODO SANITIZE
            if str(self.request.user.id) == self.kwargs['user_id']:
                return self.request.user.options.filter(question__survey__id=self.kwargs['survey_id']).all()
            if not self.request.user.options.filter(question__survey__id=self.kwargs['survey_id']).exists():
                return []
            # case other user asking survey ensure he is a friend or a match to show their survey data
            user_info_needed = Profile.objects.get(id=self.kwargs['user_id'])
            if self.request.user.friends.filter(id=user_info_needed.id).all().exists() \
                    and user_info_needed.options is not None \
                    and user_info_needed.friends.filter(id=self.request.user.id).all().exists():
                return user_info_needed.options.filter(question__survey__id=self.kwargs['survey_id'],
                                                       question__survey__visibility__in=['public', 'private']).all()
            else:
                return user_info_needed.options.filter(question__survey__id=self.kwargs['survey_id'],
                                                question__survey__visibility__in=['public']).all()

        except Profile.DoesNotExist:
            return []


class GetUserCreatedSurveys(ListAPIView):
    serializer_class = SurveySerializer

    def get_queryset(self):
        try:
            if str(self.request.user.id) == self.kwargs['pk']:
                survey = Survey.objects.filter(created_by__id=self.kwargs['pk']).all()
                return survey

            user_info_needed = Profile.objects.get(id=self.kwargs['pk'])
            if user_info_needed.friends.filter(id=self.request.user.id).all().exists() or user_info_needed.options\
                    .filter(id__in=self.request.user.options.values_list('id', flat=True)).exists():
                survey = Survey.objects.filter(created_by__id=self.kwargs['pk'],
                                               visibility__in=['public', 'private']).all()
                return survey
            else:
                return []
        except Survey.DoesNotExist:
            return []


class GetUserCompletedSurveys(ListAPIView):
    serializer_class = SurveySerializer

    def get_queryset(self):
        try:
            if str(self.request.user.id) == self.kwargs['pk']:
                completedsurveys = Survey.objects.filter(question__option__profile__exact=self.request.user).all().distinct()
                return completedsurveys

            user_info_needed = Profile.objects.get(id=self.kwargs['pk'])
            if user_info_needed.friends.filter(id=self.request.user.id).all().exists() or user_info_needed.options\
                    .filter(id__in=self.request.user.options.values_list('id', flat=True)).exists():
                surveys = Survey.objects.filter(question__option__profile__exact=self.kwargs['pk'],
                                                visibility__in=['public', 'friends']).all().distinct()
                return surveys
            else:
                return []
        except Survey.DoesNotExist:
            return []


class GetWhatFriendsHaveCompletedTheSurvey(ListAPIView):
    serializer_class = PublicProfileSerializer

    def get_queryset(self):
        return self.request.user.friends.filter(options__question__survey__id=self.kwargs['pk']).all().distinct().all()


# may be redundant if you check aready answered
class CheckIfCompleted(APIView):
    # get all friends and filter those who have completed the answer
    def get(self, *args, **kwargs):
        #self.request.user.friends
        result = Survey.objects.filter(Q(created_by=self.request.user) | Q(question__option__profile__exact=self.request.user), id=self.kwargs['pk'])
        return Response({'result': result.exists() }, status=status.HTTP_200_OK)

